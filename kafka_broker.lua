local skynet = require ("skynet")
local socketchannel = require ("socketchannel")
local response = require ("kafka_response")

local DEFAULT_HOST = "127.0.0.1"    --默认连接的kafka地址
local DEFAULT_PORT = 9092           --默认连接的kafka端口
local retryConnectInterval = 60*100 --断线重连间隔
local SOCKETSTATE = {               --socket连接状态
    INIT = 0,                       --初始化
    CONNECTED = 1,                  --已连接
    DISCONNECT = 2,                 --断开连接
    RECONNECTING = 3,               --断线重连中
}

local brokerInsCache                --broker实例缓存
local _M = {}
local mt = { __index = _M }

local function _myreadreply( sock ,flag )
    if "SEND" == flag then
        local header = sock:read(4)
        if not header then
            return nil, "failed to receive packet header"
        end
        local len = response.to_int32(header)
        local content = sock:read(len)
        if not content then
            return nil, "failed to receive packet " .. len
        end
        return true,content
    end
end

local function _query_resp(flag)
     return function(sock)
        return _myreadreply(sock, flag)
    end
end

function _M.new(self, host, port, socket_config)
    return setmetatable({
        host = host,
        port = port,
        config = socket_config,
        sock = nil,
        state = SOCKETSTATE.INIT,
        reconnetNum = 0,
    }, mt)
end

function _M.init(self)
    if self.state ~= SOCKETSTATE.INIT then
        return
    end
    self.state = SOCKETSTATE.DISCONNECT

    local connectok = self:connect()
    if connectok then
        self.state = SOCKETSTATE.CONNECTED
    else
        if self.sock then
            self.sock:close()
        end
        self.sock = nil
        self.state = SOCKETSTATE.DISCONNECT
        skynet.fork(handler(self, self.retryconnect))
    end
end

function _M.connect(self)
    local channel = socketchannel.channel({
        host = self.host or DEFAULT_HOST,
        port = self.port or DEFAULT_PORT,
        nodelay = true,
    })
    -- try connect first only once
    local xpcallok = xpcall(handler(channel, channel.connect), serviceFunctions.exception, true)
    if xpcallok then
        self.sock = channel
        Log.d("kafka_broker socketchannel connect scucess, sock:", self.sock)
    else
        self.sock = nil
        channel:close()
        channel = nil
        Log.w("kafka_broker socketchannel connect fail")
    end
    return xpcallok
end

function _M.retryconnect(self)
    if self.state == SOCKETSTATE.RECONNECTING then --已经正在断线重连,直接返回
        return
    end
    self.state = SOCKETSTATE.RECONNECTING
    self.reconnetNum = 0
    Log.i("kafka_broker retryconnect enter", self, self.host, self.port)

    if self.sock then
        self.sock:close()
        self.sock = nil
    end
    self:do_retryconnect()
end

function _M.do_retryconnect(self)
    self.reconnetNum = self.reconnetNum + 1
    local connectok = self:connect()
    if connectok then
        self.state = SOCKETSTATE.CONNECTED
        Log.i("kafka_broker retryconnect scucessed, reconnetNum:", self.reconnetNum)
        return true
    else
        Log.e("kafka_broker retryconnect failed, reconnetNum:", self.reconnetNum)
        skynet.timeout(retryConnectInterval, function ()
            self:do_retryconnect()
        end)
    end
end

function _M.send_receive(self, request)
    -- Log.d("kafka_broker send_receive:", self.sock, self.state)
    if not self.sock or self.state ~= SOCKETSTATE.CONNECTED then
        return nil, "sockect not available", true
    end

    local query_resp = _query_resp("SEND")
    local xpcallok,ret = xpcall(handler(self.sock, self.sock.request), serviceFunctions.exception, request:package(), query_resp)
    if xpcallok then
        return response:new(ret), nil, true
    else--需要断线重连
        if self.sock then
            self.sock:close()
        end
        self.sock = nil
        skynet.fork(handler(self, self.retryconnect))
        return nil, "sockect not available", true
    end
end

function _M.getBroker(host, port, socket_config)
    if not brokerInsCache then
        brokerInsCache = {}
    end
    local brokerKey = string.format("host:%s_port:%d", host, port)
    if brokerInsCache[brokerKey] then
        -- Log.d("getBroker from cache brokerKey:",brokerKey,self)
        return brokerInsCache[brokerKey]
    end
    -- Log.d("getBroker from newone brokerKey:",brokerKey,self)
    local instance = _M:new(host, port, socket_config)
    brokerInsCache[brokerKey] = instance

    return brokerInsCache[brokerKey]
end

return _M
