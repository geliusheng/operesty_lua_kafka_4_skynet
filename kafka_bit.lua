--[[
	Lua5.3以后, 已支持位运算的各种符号.
	左移:	a<<b
	右移: 	a>>b
	与:		a&b
	或:		a|b
	异或:	a~b
	非：		~a
]]

local LuaBit = {}

--左移
function LuaBit.lshift(a, b)
	return a << b
end

--右移
function LuaBit.rshift(a, b)
	return a >> b
end

--与
function LuaBit.band(a, b)
	return a & b
end

--或
function LuaBit.bor(a, b, c, d)
	local ret = a | b
	if c then
		ret = ret | c
	end
	if d then
		ret = ret | d
	end
	return ret
end

--异或
function LuaBit.bxor(a, b)
	return a ~ b
end

--非
function LuaBit.bnot(a)
	return ~a
end

return LuaBit
